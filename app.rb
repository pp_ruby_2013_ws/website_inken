#!/usr/bin/env ruby
# coding: utf-8

require 'bundler/setup'
Bundler.require
require 'sinatra'

get '/' do
  render :haml, :index
end